﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="perfil.aspx.cs" Inherits="Proyecto1.chat.perfil1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 52px;
        }
        .auto-style2 {
            height: 30px;
        }
        .auto-style3 {
            height: 34px;
        }
        .auto-style4 {
            height: 33px;
        }
        .auto-style5 {
            margin-left: 202px;
            margin-bottom: 0px;
        }
        .auto-style6 {
            margin-left: 22px;
        }
        .auto-style7 {
            margin-left: 28px;
        }
        .auto-style8 {
            margin-left: 26px;
        }
        .auto-style9 {
            margin-left: 29px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="auto-style1">
        Perfil de usuario<asp:Button ID="btnChat" runat="server" BackColor="#FF9900" CssClass="auto-style5" OnClick="btnChat_Click" Text="Ir al chat" Width="84px" />
    </h1>
    <h3 class="auto-style2">
        Usuario:
        <asp:Label ID="usuario" runat="server"></asp:Label>
    </h3>
    <h3 class="auto-style3">
        Nombre:
        <asp:Label ID="nombre" runat="server"></asp:Label>
    </h3>
    <h3 class="auto-style4">
        Apellido:
        <asp:Label ID="apellido" runat="server"></asp:Label>
    </h3>
    <p>
        <asp:Button ID="btnModifica" runat="server" BackColor="#FF9900" CssClass="auto-style6" OnClick="btnModifica_Click" Text="Modificar" Width="79px" />
        <asp:Button ID="btnContrasena" runat="server" BackColor="#FF9900" CssClass="auto-style7" OnClick="btnContrasena_Click" Text="Cambiar contraseña" Width="148px" />
        <asp:Button ID="btnPregunta" runat="server" BackColor="#FF9900" CssClass="auto-style8" OnClick="btnPregunta_Click" Text="Cambiar pregunta" Width="150px" />
        <asp:Button ID="btnEliminar" runat="server" BackColor="#FF9900" CssClass="auto-style9" OnClick="btnEliminar_Click" Text="Eliminar" Width="84px" />
    </p>
</asp:Content>
