﻿using Proyecto1.app;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto1.chat
{
    public partial class cambiarPregunta : System.Web.UI.Page
    {
        Servicio gise = new Servicio();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardarP_Click(object sender, EventArgs e)
        {
            Usuario gise_user = gise.cambioPerfil((string)Session["Usuario"], null, null, txtPreguntaN.Text.ToString(), 
                txtRespuestaN.Text.ToString());

            Response.Redirect("modificada.aspx");
        }
    }
}

//Giselle Mingue