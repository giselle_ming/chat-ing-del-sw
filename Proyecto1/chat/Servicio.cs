﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace Proyecto1.app
{   //Giselle Mingue
    //este documento contiene todos los metodos de
    //generar los XML con la informacion necesaria
    //agregar un usuario al XML (registro)
    //verificar si un usuario existe
    //verificar si la clave concuerda con el usuario
    //agregar un amigo a la lista de amigos de un usuario
    //eliminar un usuario
    //agregar todos los mensajes de un usuario cuando este no este conectado
    //agregar la pregunta y respuesta secreta
    //cambiar datos del usuario como contrasena, pregunta y respuesta secreta
    //OPCIONAL
    //mantener dos lista de amigos, una de amigos bloqueados y desbbloqueados
    //mantener el estado del usuario (conectado, ocupado, no disponible, desconectado)
    public class Servicio
    {

        static string pathString = @"C:\Users\Gi\Documents\T-conte";
        string users_xml_path = @"C:\Users\Gi\Documents\T-conte\users.xml";
        string mensajes_xml_path = @"C:\Users\Gi\Documents\T-conte\mensajes.xml";




        //////////////////////////////////////////////////////////////////////////////////////
        //La funcion crea una carpeta llamada en el directorio 
        //C:\Users\[User_name]\AppData\Local\T-conte en caso de que no exista
        //en dicha carpeta se almacenara los XML con toda la 
        //informacion de los usuarios y del chat
        //en caso de que se cree la carpeta se creara dos documentos XML en blanco
        //uno de los XML llamado server.xml mantendra informacion del servidor como todos los usuarios actuales
        //(solo nombres de los usuarios), mientras que el otro llamado users.xml mantendra informacion de cada
        //usuario
        //Parametros:
        //
        //Retorno:
        //    0 -> la carpeta no existia y se creo con exito
        //    1 -> la carpeta existe y los documentos XML no existian
        //    2 -> la carpeta existe los documentos XML no existian y se crearon
        //    -1 -> no se pudo crear la carpeta o los documentos (puede ser que no exista o no se
        //           tienen los permisos necesarios en dicho directorio)
        //////////////////////////////////////////////////////////////////////////////////////
        public int crearCarpeta()
        {
            //si existe = true el directorio T-conte existe, en caso contrario no existe
            bool existe = true;

            //crea el direcorio T-conte en la direccion dada en caso de que no exista
            if (!System.IO.Directory.Exists(pathString))
            {
                System.Diagnostics.Debug.WriteLine("el directorio " + pathString + " no existe");
                //se crea el direcotrio T-conte debido a que no existia
                try
                {
                    System.Diagnostics.Debug.WriteLine("creando el directorio " + pathString);
                    System.IO.Directory.CreateDirectory(pathString);
                }
                catch
                {
                    //no se pudo crear el direcotrio T-conte
                    System.Diagnostics.Debug.WriteLine("error creando " + pathString + " el directorio pedido no existe o no se tiene permisos para escribir");
                    return -1;
                }
                existe = false;
            }

            //verifica que users.xml existe
            if (!System.IO.File.Exists(users_xml_path) || !System.IO.File.Exists(mensajes_xml_path))
            {

                System.Diagnostics.Debug.WriteLine("los archivos archivo " + users_xml_path + ", " + mensajes_xml_path + " no existen");

                //creo la instancia de XML
                XmlDocument xml_doc_user = new XmlDocument();
                XmlDocument xml_doc_mensajes = new XmlDocument();

                //se le agrega la codificacion utf8 al documento -> <?xml version="1.0" encoding="utf-8"?>
                XmlNode node_user = xml_doc_user.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlNode node_mensajes = xml_doc_mensajes.CreateXmlDeclaration("1.0", "UTF-8", null);
                xml_doc_user.AppendChild(node_user);
                xml_doc_mensajes.AppendChild(node_mensajes);

                //crear el nodo o etiqueta raiz, 
                XmlNode nodo_raiz_user = xml_doc_user.CreateElement("t-conte");
                XmlElement nodo_raiz_mensajes = xml_doc_mensajes.CreateElement("t-conte");
                nodo_raiz_user.InnerText = "";

                nodo_raiz_mensajes.SetAttribute("mensajes", "0");
                xml_doc_mensajes.AppendChild(nodo_raiz_mensajes);

                //agregar la etiqueta raiz a ambos XML
                xml_doc_user.AppendChild(nodo_raiz_user);

                //salva los XML en las rutas en especifica
                try
                {
                    System.Diagnostics.Debug.WriteLine("creando " + users_xml_path);
                    xml_doc_user.Save(users_xml_path);
                    System.Diagnostics.Debug.WriteLine("se creo " + users_xml_path);
                    System.Diagnostics.Debug.WriteLine("creando " + mensajes_xml_path);
                    xml_doc_mensajes.Save(mensajes_xml_path);
                }
                catch
                {
                    //no se pudo crear el XML
                    System.Diagnostics.Debug.WriteLine("no se pudo crear " + users_xml_path + " no se tiene permisos en el directorio de escritura o no existe el directrio");
                    return -1;
                }


                //verifica si la carpeta se tuvo que crear o no
                if (!existe)
                    return 0;
                else
                    return 2;

            }
            else
            {
                //el documento info.xml ya existia en la carpeta en especifica
                System.Diagnostics.Debug.WriteLine("el archivo " + users_xml_path + " existe, no hay necesidad de crearlo");
                return 1;
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //esta funcion crea un usuario, esta funcion asume que todos los parametros son validos
        //y no busca verificar la contrasena, o la fecha de nacimiento, ect, solo crea el usuario en caso de que
        //no exista
        //Parametros:
        //  nombre Nombre del usuario
        //  apellido Apellido del usuario
        //  usuario Usuario
        //  contrasena Contrasena
        //  nacimiento Nacimiento del usuario
        //  fechaCreacion Fecha de la creacion de la cuenta (fecha actual del servidor)
        //  preguntaSecreta pregunta secreta escogida por el usuario
        //  respuestaSecreta Respuesta secreta escogida por el usuario
        //Retorno:
        //  una instalcia de usuario en caso de que se pueda creara
        //  null            en caso de que el usuario exista
        //  Usuario.Error   una instancia de Usuario con un campo de Usuario.Error != null en caso de error
        //                  que no se pudo leer el XML (no existe o la direccion esta mal)
        //////////////////////////////////////////////////////////////////////////////////////
        public Usuario crearUsuario(string nombre, string apellido, string usuario, string contrasena,
                                    string preguntaSecreta, string respuestaSecreta)
        {

            bool usuario_existe = false;

            //lee el XML
            XmlDocument xml_users = new XmlDocument();
            try
            {
                System.Diagnostics.Debug.WriteLine("leyendo el archivo " + users_xml_path);
                xml_users.Load(users_xml_path);
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("no se pudo leer el archivo " + users_xml_path + " no existe o no hay permisos de lectura");
                Usuario error = new Usuario();
                error.Error = "error leyendo";
                return error;
            }

            //busca el nodo de la etiqueta <usuario> para obtener todos los usuarios
            XmlNodeList lista_usuarios = xml_users.SelectNodes("//t-conte/usuario");

            if (lista_usuarios != null)
            {
                foreach (XmlNode usuario_unico in lista_usuarios)
                {
                    if (usuario_unico["nombre-usuario"] == null)
                    {
                        //nada
                    }
                    else if (usuario_unico["nombre-usuario"].InnerText == usuario)
                    {

                        usuario_existe = true;
                        break;
                    }
                }
            }


            //en caso de que exista el usuario se crea una variable de usuario con todos sus campos en null
            if (usuario_existe)
            {
                System.Diagnostics.Debug.WriteLine("el usuario " + usuario + " existe");
                return null;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("el usuario " + usuario + " no existe, creando el usuario con todos los campos dados");
                //en caso de que no exista el suaurio se debe agregar el usuario user.xml

                //modificar users.xml
                //se agrega una etiqueta 
                /*
                <usuario>
                    <nombre></nombre>
                    <apellido></apellido>
                    <usuario></usuario>
                    <contrasena></contrasena>
                    <fecha-creacion></fecha-creacion>
                    <ultima-conexion></ultima-conexion>
                    <pregunta-secreta></pregunta-secreta>
                    <respuesta-secreta></respuesta-secreta>
                </usuario>
                */
                //lee el XML
                XmlNode nodo_raiz_user = xml_users.SelectSingleNode("//t-conte");
                XmlNode nodo_usuario = xml_users.CreateElement("usuario");
                nodo_raiz_user.AppendChild(nodo_usuario);

                XmlNode nodo_nombre = xml_users.CreateElement("nombre");
                nodo_nombre.InnerText = nombre;
                nodo_usuario.AppendChild(nodo_nombre);

                XmlNode nodo_apellido = xml_users.CreateElement("apellido");
                nodo_apellido.InnerText = apellido;
                nodo_usuario.AppendChild(nodo_apellido);

                XmlNode nodo_usuario_nombre = xml_users.CreateElement("nombre-usuario");
                nodo_usuario_nombre.InnerText = usuario;
                nodo_usuario.AppendChild(nodo_usuario_nombre);

                XmlNode nodo_contrasena = xml_users.CreateElement("contrasena");
                nodo_contrasena.InnerText = contrasena;
                nodo_usuario.AppendChild(nodo_contrasena);


                XmlNode nodo_pregunta_secreta = xml_users.CreateElement("pregunta-secreta");
                nodo_pregunta_secreta.InnerText = preguntaSecreta;
                nodo_usuario.AppendChild(nodo_pregunta_secreta);

                XmlNode nodo_respuesta_secreta = xml_users.CreateElement("respuesta-secreta");
                nodo_respuesta_secreta.InnerText = respuestaSecreta;
                nodo_usuario.AppendChild(nodo_respuesta_secreta);

                try
                {
                    xml_users.Save(users_xml_path);
                }
                catch
                {
                    System.Diagnostics.Debug.WriteLine("no se pudo guardar el archivo " + users_xml_path + " no existe el directorio o no hay permisos de lectura");
                    Usuario error = new Usuario();
                    error.Error = "error escribiendo";
                    return error;
                }


                Usuario usuarioNuevo = new Usuario();
                usuarioNuevo.Nombre = nombre;
                usuarioNuevo.Apellido = apellido;
                usuarioNuevo.UsuarioNombre = usuario;
                usuarioNuevo.Contrasena = contrasena;
                usuarioNuevo.PreguntaSecreta = preguntaSecreta;
                usuarioNuevo.RespuestaSecreta = respuestaSecreta;
                return usuarioNuevo;
            }
        }
        
        //funcion que revisa si el usuario existe o no, en caso de que exista retorna una instancia de Usuario con todos los datos
        //en caso de que no retorna null
        //parametros
        //  usuario  el nombre del usuario en string
        //  contrasena    la contrasena del usuario a buscar
        //retorna 
        //  Usuario   instancia de la clase Usuario en caso de exito
        //  null     en caso de que no encontro al usuario en el XML
        //  Usuario.Error   una instancia de Usuario con un campo de Usuario.Error != null en caso de error
        //                  que no se pudo leer el XML (no existe o la direccion esta mal)
        public Usuario verificarUsuario(string usuario, string contrasena)
        {

            //lee el XML
            XmlDocument xml_server = new XmlDocument();
            try
            {
                xml_server.Load(users_xml_path);
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("no se pudo leer el archivo " + users_xml_path + " no existe o no hay permisos de lectura");
                Usuario error = new Usuario();
                error.Error = "error";
                return error;
            }

            //busca el nodo de la etiqueta <usuario> para obtener todos los usuarios
            XmlNodeList lista_usuarios = xml_server.SelectNodes("//t-conte/usuario");

            if (lista_usuarios != null)
            {
                foreach (XmlNode usuario_unico in lista_usuarios)
                {
                    if (usuario_unico["nombre-usuario"] == null)
                    {
                        //nada
                    }
                    else if (usuario_unico["nombre-usuario"].InnerText == usuario && usuario_unico["contrasena"].InnerText == contrasena)
                    {
                        System.Diagnostics.Debug.WriteLine("usuario " + usuario + " es valido");
                        Usuario usuario_encontrado = new Usuario();
                        usuario_encontrado.Nombre = usuario_unico["nombre"].InnerText;
                        usuario_encontrado.Apellido = usuario_unico["apellido"].InnerText;
                        usuario_encontrado.UsuarioNombre = usuario_unico["nombre-usuario"].InnerText;
                        usuario_encontrado.Contrasena = usuario_unico["contrasena"].InnerText;
                        usuario_encontrado.PreguntaSecreta = usuario_unico["pregunta-secreta"].InnerText;
                        usuario_encontrado.RespuestaSecreta = usuario_unico["respuesta-secreta"].InnerText;
                        return usuario_encontrado;
                    }

                }
            }
            System.Diagnostics.Debug.WriteLine("usuario " + usuario + " no es valido, no existe o la contraseña es invalida");
            //no se encontro el usuario o la contraseña es invalida
            return null;
        }
        
        //funcion que retorna toda la informacion de un usuario al recibir el usuario
        //en caso de que no retorna null
        //parametros
        //  usuario  el nombre del usuario en string
        //retorna 
        //  Usuario   instancia de la clase Usuario en caso de exito
        //  null     en caso de que no encontro al usuario en el XML o no logre abrir el XML
        //  Usuario.Error   una instancia de Usuario con un campo de Usuario.Error != null en caso de error
        //                  que no se pudo leer el XML (no existe o la direccion esta mal)
        public Usuario getInfoUsuario(string usuario)
        {

            //lee el XML
            XmlDocument xml_server = CargarXML(users_xml_path);
            if (xml_server == null)
            {
                //error leyendo XML
                Usuario error = new Usuario();
                error.Error = "error";
                return error;
            }

            XmlNode usuario_unico = BuscarUsuario(xml_server, usuario);
            if (usuario_unico == null)
            {
                System.Diagnostics.Debug.WriteLine("usuario " + usuario + " no existe");
                //no enocntro al usuario
                return null;
            }
            System.Diagnostics.Debug.WriteLine("usuario " + usuario + " es valido");
            Usuario usuario_encontrado = new Usuario();
            usuario_encontrado.Nombre = usuario_unico["nombre"].InnerText;
            usuario_encontrado.Apellido = usuario_unico["apellido"].InnerText;
            usuario_encontrado.UsuarioNombre = usuario_unico["nombre-usuario"].InnerText;
            usuario_encontrado.Contrasena = usuario_unico["contrasena"].InnerText;
            usuario_encontrado.PreguntaSecreta = usuario_unico["pregunta-secreta"].InnerText;
            usuario_encontrado.RespuestaSecreta = usuario_unico["respuesta-secreta"].InnerText;

            return usuario_encontrado;


        }
        
        //funcion que borra toda la informacion de un usuario en el XML
        //parametros
        //  usuario  el nombre del usuario en string
        //retorna 
        //  true    en caso de que se borre con exito el usuario
        //  false    en caso de que no se pueda borrar por alguna razon (no existe o no puede leer el XML)
        public bool borrarUsuario(string usuario)
        {

            //lee el XML
            XmlDocument xml_server = CargarXML(users_xml_path);
            if (xml_server == null)
            {
                //error leyendo XML
                return false;
            }
            try
            {
                XmlNode usuario_unico = BuscarUsuario(xml_server, usuario);
                if (usuario_unico == null)
                {
                    return false;
                }
                System.Diagnostics.Debug.WriteLine("se tratara de borrar el usuario " + usuario);
                //usuario_unico.RemoveAll();
                usuario_unico.ParentNode.RemoveChild(usuario_unico);
                xml_server.Save(users_xml_path);
                return true;
            }
            catch
            {   //no se pudo leer o guardar el documento XML
                System.Diagnostics.Debug.WriteLine("no se pudo leer o guardar el archivo " + users_xml_path + " no existe o no hay permisos de lectura o escritura");
                return false;
            }
        }
        //funcion que cambia la contrasena de un usuario, esta funcion primero verifica que el usuario y la contrasena
        //vieja sean validos
        //parametros
        //  usuario  el nombre del usuario en string
        //  contrasena    la contrasena del usuario a buscar
        //  contrasena_nueva   la contrasena nueva del usuario
        //retorna 
        //  Usuario   instancia de la clase Usuario en caso de exito
        //  null     en caso de que la contrasena vieja sea equivocada o el usuario erroneo
        //  Usuario.Error   una instancia de Usuario con un campo de Usuario.Error != null en caso de error
        //                  que no se pudo leer el XML (no existe o la direccion esta mal)
        public Usuario cambiarContrasena(string usuario, string contrasena, string nueva_contrasena)
        {

            XmlDocument xml_server = CargarXML(users_xml_path);
            if (xml_server == null)
            {
                //error leyendo XML
                Usuario error = new Usuario();
                error.Error = "error";
                return error;
            }

            XmlNode usuario_unico = BuscarUsuario(xml_server, usuario);
            if (usuario_unico == null)
            {
                return null;
            }

            if (usuario_unico["nombre-usuario"].InnerText == usuario && usuario_unico["contrasena"].InnerText == contrasena)
            {
                usuario_unico["contrasena"].InnerText = nueva_contrasena;
                Usuario usuario_nueva_contrasena = getInfoUsuario(usuario);
                try
                {
                    xml_server.Save(users_xml_path);
                }
                catch
                {
                    //error escribiendo XML
                    Usuario error = new Usuario();
                    error.Error = "error";
                    return error;
                }
                return usuario_nueva_contrasena;
            }


            System.Diagnostics.Debug.WriteLine("usuario " + usuario + " no es valido, no existe o la contraseña es invalida");
            //no se encontro el usuario o la contraseña es invalida
            return null;
        }
        
        /// <summary>
        /// Funcion que modifica los parametros de nombre apellido pregunta y respuesta secreta de un usuario
        /// en caso de que no se desee modificar cierto parametro se coloca un null en dicho campo
        /// ejemplo cambioPerfil(string usuario, null, null, pregunta, respuesta);
        /// solo cambia la pregunta y respuesta, el campo usuario es obligatorio
        /// </summary>
        /// <param name="usuario">nombre de usuario</param>
        /// <param name="nombre">nombre nuevo</param>
        /// <param name="apellido">apellido nuevo</param>
        /// <param name="pregunta">pregunta nueva</param>
        /// <param name="respuesta">respuesta nueva</param>
        /// <returns>
        /// una  instancia de Usuario con sus campos distinto de null en caso de exito
        /// null en caso de que no se consiga el usuario
        /// una instancia de Usuario con Usuario.error != null con un error de lectura o escritura
        /// </returns>
        public Usuario cambioPerfil(string usuario, string nombre, string apellido,
                                    string pregunta, string respuesta)
        {

            XmlDocument xml_server = CargarXML(users_xml_path);
            if (xml_server == null)
            {
                //error leyendo XML
                Usuario error = new Usuario();
                error.Error = "error";
                return error;
            }

            XmlNode usuario_unico = BuscarUsuario(xml_server, usuario);
            if (usuario_unico == null)
            {
                System.Diagnostics.Debug.WriteLine("usuario " + usuario + " no es valido, no existe o la contraseña es invalida");
                //no se encontro el usuario o la contraseña es invalida
                return null;
            }

            //cambia los parametros
            if (nombre != null)
                usuario_unico["nombre"].InnerText = nombre;
            if (apellido != null)
                usuario_unico["apellido"].InnerText = apellido;
            if (pregunta != null)
                usuario_unico["pregunta-secreta"].InnerText = pregunta;
            if (respuesta != null)
                usuario_unico["respuesta-secreta"].InnerText = respuesta;

            try
            {
                xml_server.Save(users_xml_path);
            }
            catch
            {
                //error escribiendo XML
                Usuario error = new Usuario();
                error.Error = "error";
                return error;
            }

            Usuario usuario_nuevo_perfil = getInfoUsuario(usuario);
            return usuario_nuevo_perfil;

        }
        

        /// <summary>
        /// Esta funcion agrega un mensaje en el xml mensajes.xml
        /// </summary>
        /// <param name="usuario">usuario que envio el mensaje</param>
        /// <param name="mensaje">mensaje que envio el usuario</param>
        /// <param name="date">la fecha como DateTime.Now</param>
        /// <returns>true en caso de que se logro guardar de manera exitosa el mensaje en el XML
        ///             false en caso de que no se pudo guardar por error de escritura o no existe el XML</returns>
        public bool agregarMensaje(string usuario, string mensaje, DateTime date)
        {
            //cargar el mensajes.xml
            XmlDocument xml_server = CargarXML(mensajes_xml_path);
            if (xml_server == null)
            {
                //error leyendo XML
                return false;
            }

            //se busca los numeros de mensajes guardados en el atributo mensajes
            int num_mensajes = Int32.Parse(xml_server.SelectSingleNode("//t-conte/@mensajes").Value);
            System.Diagnostics.Debug.WriteLine("hay " + num_mensajes + " mensajes");
            if (num_mensajes < 200)
            {
                num_mensajes++;

                XmlNode raiz = xml_server.SelectSingleNode("//t-conte");
                if (raiz == null)
                {
                    //error en el XML no hay etiqueta t-conte
                    return false;
                }
                XmlElement nodo_mensaje = xml_server.CreateElement("mensaje");

                raiz.Attributes["mensajes"].Value = Convert.ToString(num_mensajes);
                guardarMensaje(xml_server, raiz, usuario, mensaje, date);
                System.Diagnostics.Debug.WriteLine("mensaje guardado");
                return true;

            }
            else
            {
                XmlNode raiz = xml_server.SelectSingleNode("//t-conte");
                if (raiz == null)
                {
                    //error en el XML no hay etiqueta t-conte
                    return false;
                }
                raiz.RemoveChild(raiz.FirstChild);
                System.Diagnostics.Debug.WriteLine("primer elemento del XML eliminado");
                guardarMensaje(xml_server, raiz, usuario, mensaje, date);
                System.Diagnostics.Debug.WriteLine("mensaje guardado");
                return true;
            }



        }
        

        /// <summary>
        /// funcion que retorna el ultimo mensaje del xml mensajes.xml
        /// </summary>
        /// <returns>un string con el mensaje a mostrar de la forma 
        /// [nombre_usuario  DD/MM/YYYY  hora]: mensaje  
        /// en caso de error a la hora de leer el XML retorna un null </returns>
        /// 
        public string ultimoMensaje()
        {
            //cargar el mensajes.xml
            XmlDocument xml_server = CargarXML(mensajes_xml_path);
            if (xml_server == null)
            {
                //error leyendo XML
                return null;
            }

            XmlNode raiz = xml_server.SelectSingleNode("//t-conte");

            if (raiz == null)
            {
                //error en el XML no hay etiqueta t-conte
                return null;
            }
            XmlNode ultimo_mensaje = raiz.LastChild;
            System.Diagnostics.Debug.WriteLine("ultimo mensaje en XML");
            if (ultimo_mensaje == null)
            {
                //error en el XML no hay etiquetas
                return null;
            }

            return mensajeXmlToString(ultimo_mensaje);


        }

        /// <summary>
        /// funcion que retorna todos los mensajes en una lista de string del XML mensaje.xml
        /// </summary>
        /// <returns>retorna todos los mensajes en una lista, cada mensaje con la forma
        /// [nombre_usuario  DD/MM/YYYY  hora]: mensaje  
        /// en caso de error a la hora de leer el XML retorna un null </returns>
  
        public List<string> todosMensajes()
        {

            //cargar el mensajes.xml
            XmlDocument xml_server = CargarXML(mensajes_xml_path);
            if (xml_server == null)
            {
                //error leyendo XML
                return null;
            }

            List<string> mensajes = new List<string>();

            //se buscan todos los nodos o etiquetas de <mensaje>
            XmlNodeList mensajes_xml = xml_server.SelectNodes("//t-conte/mensaje");

            if (mensajes_xml == null)
            {
                //error en el XML no hay etiquetas de mensaje
                return null;
            }

            //para cada etiqueta de mensaje se almacena en la lista
            foreach (XmlNode mensaje_xml in mensajes_xml)
            {
                System.Diagnostics.Debug.WriteLine("agregando mensaje al XML ");
                mensajes.Add(mensajeXmlToString(mensaje_xml));
            }

            return mensajes;

        }


        //funcion que carga el XML
        private XmlDocument CargarXML(string path)
        {
            //lee el XML
            XmlDocument xml_server = new XmlDocument();
            try
            {
                xml_server.Load(path);
                return xml_server;
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("no se pudo leer el archivo " + path + " no existe o no hay permisos de lectura");
                return null;
            }
        }

        //funcion que busca en el XML un usuario
        private XmlNode BuscarUsuario(XmlDocument xml_server, string usuario)
        {
            //busca el nodo de la etiqueta <usuario> para obtener todos los usuarios
            XmlNodeList lista_usuarios = xml_server.SelectNodes("//t-conte/usuario");

            if (lista_usuarios != null)
            {
                foreach (XmlNode usuario_unico in lista_usuarios)
                {
                    if (usuario_unico["nombre-usuario"] == null)
                    {
                        //nada
                    }
                    else if (usuario_unico["nombre-usuario"].InnerText == usuario)
                    {
                        return usuario_unico;
                    }

                }
            }

            return null;
        }

        private void guardarMensaje(XmlDocument xml_server, XmlNode raiz, string usuario, string mensaje, DateTime date)
        {

            XmlElement nodo_mensaje = xml_server.CreateElement("mensaje");

            //crear tag usuario
            XmlNode nodo_usuario = xml_server.CreateElement("usuario");
            nodo_usuario.InnerText = usuario;
            nodo_mensaje.AppendChild(nodo_usuario);

            //crea tag mensaje
            XmlNode nodo_mensaje_enviar = xml_server.CreateElement("mensaje-enviar");
            nodo_mensaje_enviar.InnerText = mensaje;
            nodo_mensaje.AppendChild(nodo_mensaje_enviar);

            //crear tag usuario
            XmlNode nodo_dia = xml_server.CreateElement("dia");
            nodo_dia.InnerText = Convert.ToString(date.Day); ;
            nodo_mensaje.AppendChild(nodo_dia);

            //crea tag mensaje
            XmlNode nodo_mes = xml_server.CreateElement("mes");
            nodo_mes.InnerText = Convert.ToString(date.Month);
            nodo_mensaje.AppendChild(nodo_mes);

            //crear tag usuario
            XmlNode nodo_ano = xml_server.CreateElement("ano");
            nodo_ano.InnerText = Convert.ToString(date.Year);
            nodo_mensaje.AppendChild(nodo_ano);

            //crea tag mensaje
            XmlNode nodo_hora = xml_server.CreateElement("hora");
            nodo_hora.InnerText = Convert.ToString(date.Hour) + ":" + Convert.ToString(date.Minute);
            nodo_mensaje.AppendChild(nodo_hora);

            raiz.AppendChild(nodo_mensaje);
            xml_server.AppendChild(raiz);


            xml_server.Save(mensajes_xml_path);
        }

        public string mensajeXmlToString(XmlNode xml_mensaje)
        {
            Mensajes mensaje = new Mensajes();
            mensaje.Mensaje = xml_mensaje["mensaje-enviar"].InnerText;
            mensaje.Usuario = xml_mensaje["usuario"].InnerText;
            mensaje.Dia = xml_mensaje["dia"].InnerText;
            mensaje.Mes = xml_mensaje["mes"].InnerText;
            mensaje.Ano = xml_mensaje["ano"].InnerText;
            mensaje.Hora = xml_mensaje["hora"].InnerText;
            string mensaje_completo = "[" + mensaje.Usuario + "  " + mensaje.Dia + "/" + mensaje.Mes + "/"
                + mensaje.Ano + "  " + mensaje.Hora + "]: " + mensaje.Mensaje;
            System.Diagnostics.Debug.WriteLine("mensaje =" + mensaje_completo);
            return mensaje_completo;
        }
    }

    //Giselle Mingue
}
