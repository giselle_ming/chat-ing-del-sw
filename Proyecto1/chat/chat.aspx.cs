﻿using Proyecto1.app;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Proyecto1.chat
{
    public partial class chat : System.Web.UI.Page
    {
        Servicio gise = new Servicio();

        protected void Page_Load(object sender, EventArgs e)
        {
  
                    DataTable dataTable = new DataTable();
                    List<string> mensajes = gise.todosMensajes();

                    foreach (string cada_mensaje in mensajes)
                    {
                       // dataTable.Columns.Add(cada_mensaje);
                        listaMsg.Items.Add(cada_mensaje);
                      }
                    ViewState["listaMsg"] = dataTable;

                    listaMsg.SelectedIndex = listaMsg.Items.Count - 1; // acomodar este detallito

        }
        protected void btnAddClick(object sender, EventArgs e)
        {

        
            DateTime localDate = DateTime.Now;

            if (txtMsg.Text.ToString() == "")
            {
                errorMsg.Text = "Debe enviar un mensaje no vacio";
                txtMsg.Focus();
            }
            else
            {
                gise.crearCarpeta();
                gise.agregarMensaje((string)Session["Usuario"], txtMsg.Text.ToString(), localDate);
                listaMsg.Items.Add(gise.ultimoMensaje());
                errorMsg.Text = "";
                txtMsg.Text = "";
                txtMsg.Focus();
            }
        }
    }
}

//Giselle Mingue