﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="cambiarPregunta.aspx.cs" Inherits="Proyecto1.chat.cambiarPregunta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 43px;
        }
        .auto-style2 {
            height: 32px;
        }
        .auto-style3 {
            height: 41px;
        }
        .auto-style4 {
            margin-left: 16px;
        }
        .auto-style5 {
            margin-left: 59px;
        }
        .auto-style6 {
            margin-left: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="auto-style1">
        Cambiar pregunta</h1>
    <h3 class="auto-style2">
        Pregunta nueva:<asp:TextBox ID="txtPreguntaN" runat="server" CssClass="auto-style4" Width="680px" MaxLength="40"></asp:TextBox>
        ?<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPreguntaN" ErrorMessage="Ingresa la pregunta." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPreguntaN" ErrorMessage="Solo se admiten letras." ForeColor="Red" ValidationExpression="^[a-zA-Z ]*$">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style3">
        Respuesta:<asp:TextBox ID="txtRespuestaN" runat="server" CssClass="auto-style5" Width="380px" MaxLength="20"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRespuestaN" ErrorMessage="Ingresa la respuesta." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtRespuestaN" ErrorMessage="Solo se admiten letras." ForeColor="Red" ValidationExpression="^[a-zA-Z ]*$">*</asp:RegularExpressionValidator>
    </h3>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
    <p>
        <asp:Button ID="btnGuardarP" runat="server" BackColor="#FF9900" CssClass="auto-style6" OnClick="btnGuardarP_Click" Text="Guardar" Width="87px" />
    </p>
</asp:Content>
