﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="registro.aspx.cs" Inherits="Proyecto1.chat.registro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        height: 32px;
    }
    .auto-style2 {
        height: 33px;
    }
    .auto-style3 {
        height: 31px;
    }
    .auto-style4 {
        height: 30px;
        width: 1114px;
    }
    .auto-style5 {
        height: 44px;
    }
    .auto-style6 {
        margin-left: 27px;
    }
        .auto-style9 {
            margin-left: 3px;
        }
        .auto-style10 {
            margin-left: 6px;
        }
        .auto-style11 {
            margin-left: 1px;
        }
        .auto-style12 {
            margin-left: 0px;
        }
        .auto-style13 {
            height: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Registrate</h1>
    <p>
        &nbsp;</p>
    <h3 class="auto-style1">
        Nombre:
        <asp:TextBox ID="txtNombreR" runat="server" CssClass="auto-style12" MaxLength="10"></asp:TextBox>
    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNombreR" ErrorMessage="Ingrese nombre." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNombreR" ErrorMessage="Solo se admiten letras." ValidationExpression="[a-zA-Z ]*" ForeColor="Red">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style2">
        Apellido:
        <asp:TextBox ID="txtApellidoR" runat="server" CssClass="auto-style12" MaxLength="10"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApellidoR" ErrorMessage="Ingrese apellido." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtApellidoR" ErrorMessage="Solo se admiten letras." ValidationExpression="[a-zA-Z ]*" ForeColor="Red">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style3">
        Usuario:
        <asp:TextBox ID="txtUsuarioR" runat="server" CssClass="auto-style9" MaxLength="10"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsuarioR" ErrorMessage="Ingrese usuario." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtUsuarioR" ErrorMessage="Caracteres invalidos." ValidationExpression="[A-Za-z a-ñ 1-9]*$" ForeColor="Red">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style1">
        Contraseña:
        <asp:TextBox ID="txtContrasenaR" runat="server" CssClass="auto-style10" MaxLength="10" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtContrasenaR" ErrorMessage="Ingrese contraseña." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtContrasenaR" ErrorMessage="Solo se admiten numeros." ValidationExpression="([0-9]|-)*" ForeColor="Red">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style1">
        Confirma contraseña:
        <asp:TextBox ID="txtConfirmaR" runat="server" MaxLength="10" TextMode="Password"></asp:TextBox>
        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtContrasenaR" ControlToValidate="txtConfirmaR" ErrorMessage="Contraseña diferente" ForeColor="Red">*</asp:CompareValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtConfirmaR" ErrorMessage="Confirme contraseña." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtConfirmaR" ErrorMessage="Solo se admiten numeros." ValidationExpression="([0-9]|-)*" ForeColor="Red">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style3">
        Pregunta secreta:
        <asp:TextBox ID="txtPreguntaR" runat="server" Width="680px" MaxLength="40"></asp:TextBox>
        ?<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPreguntaR" ErrorMessage="Ingrese una pregunta." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtPreguntaR" ErrorMessage="Solo se admiten letras." ValidationExpression="^[a-zA-Z ]*$" ForeColor="Red">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style4">
        Respuesta: <asp:TextBox ID="txtRespuestaR" runat="server" Width="380px" CssClass="auto-style11" MaxLength="20"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtRespuestaR" ErrorMessage="Ingrese una respuesta." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtRespuestaR" ErrorMessage="Solo se admiten letras." ValidationExpression="^[a-zA-Z ]*$" ForeColor="Red">*</asp:RegularExpressionValidator>
    </h3>
    <p class="auto-style13">
        &nbsp;</p>
    <p>
        <asp:Label ID="cuentaExiste" runat="server" ForeColor="Red"></asp:Label>
    </p>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
<p class="auto-style5">
        <asp:Button ID="botonRegistro" runat="server" BackColor="#FF9900" CssClass="auto-style6" Text="Registrar" Width="118px" OnClick="botonRegistro_Click" />
</p>
</asp:Content>
