﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="modificaUser.aspx.cs" Inherits="Proyecto1.chat.modificaUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 44px;
        }
        .auto-style2 {
            height: 33px;
        }
        .auto-style3 {
            height: 42px;
        }
        .auto-style4 {
            margin-left: 11px;
        }
        .auto-style5 {
            margin-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="auto-style1">
        Modificar usuario</h1>
    <h3 class="auto-style2">
        Nombre nuevo:<asp:TextBox ID="txtNombre" runat="server" CssClass="auto-style4" Width="133px" MaxLength="10"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNombre" ErrorMessage="Debe ingresar nombre." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNombre" ErrorMessage="Debe ingresar solo letras." ForeColor="Red" ValidationExpression="[a-zA-Z ]*">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style3">
        Apellido nuevo:<asp:TextBox ID="txtApellido" runat="server" CssClass="auto-style5" Width="135px" MaxLength="10"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApellido" ErrorMessage="Debe ingresar el apellido." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtApellido" ErrorMessage="Debe ingresar solo letras." ForeColor="Red" ValidationExpression="[a-zA-Z ]*">*</asp:RegularExpressionValidator>
    </h3>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
    <p>
        <asp:Button ID="botonModifica" runat="server" BackColor="#FF9900" OnClick="botonModifica_Click" Text="Continuar" />
    </p>
</asp:Content>
