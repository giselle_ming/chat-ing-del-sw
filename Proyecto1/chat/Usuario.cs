﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto1.app
{
    //Giselle Mingue
    //Esta es la clase contiene toda la informacion de un usuario
    //Para crear un objeto usuario con toda la informacion del XML
    public class Usuario{

        private string nombre;
        private string apellido;
        private string usuarioNombre;
        private string contrasena;
        private string preguntaSecreta;
        private string respuestaSecreta;
        private string error;  //Se agrega un error en caso de que en una funcion exista algun error
                                //Esta variable no se agrega en ningun XML

        public Usuario(){
            this.error = null;
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public string UsuarioNombre
        {
            get { return usuarioNombre; }
            set { usuarioNombre = value; }
        }

        public string Contrasena
        {
            get { return contrasena; }
            set { contrasena = value; }
        }

        public string PreguntaSecreta
        {
            get { return preguntaSecreta; }
            set { preguntaSecreta = value; }
        }

        public string RespuestaSecreta
        {
            get { return respuestaSecreta; }
            set { respuestaSecreta = value; }
        }

        public string Error
        {
            get { return error; }
            set { error = value; }
        }


    }
}