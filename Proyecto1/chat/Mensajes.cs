﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto1.app
{
    public class Mensajes
    {
        private string mensaje;
        private string usuario;
        private string dia;
        private string mes;
        private string ano;
        private string hora;

        //Giselle Mingue

        public Mensajes()
        {
            
        }

        public string Mensaje
        {
            get { return mensaje; }
            set { mensaje = value; }
        }

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        public string Dia
        {
            get { return dia; }
            set { dia = value; }
        }

        public string Mes
        {
            get { return mes; }
            set { mes = value; }
        }

        public string Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        public string Hora
        {
            get { return hora; }
            set { hora = value; }

        }
    }
}