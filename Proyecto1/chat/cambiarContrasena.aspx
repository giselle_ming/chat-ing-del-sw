﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="cambiarContrasena.aspx.cs" Inherits="Proyecto1.chat.cambiarContrasena" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 48px;
        }
        .auto-style2 {
            height: 32px;
        }
        .auto-style3 {
            height: 31px;
        }
        .auto-style4 {
            height: 43px;
        }
        .auto-style5 {
            margin-left: 35px;
        }
        .auto-style7 {
            margin-left: 14px;
        }
        .auto-style8 {
            margin-left: 0px;
        }
        .auto-style9 {
            margin-left: 34px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="auto-style1">
        Cambiar contraseña</h1>
    <h3 class="auto-style2">
        Contraseña actual:<asp:TextBox ID="txtActual" runat="server" CssClass="auto-style5" MaxLength="10" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtActual" ErrorMessage="Escriba su contraseña actual." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtActual" ErrorMessage="Solo se admiten numeros." ForeColor="Red" ValidationExpression="([0-9]|-)*">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style3">
        Contraseña nueva:<asp:TextBox ID="txtNueva" runat="server" CssClass="auto-style9" MaxLength="10" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNueva" ErrorMessage="Escriba su contraseña actual." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtNueva" ErrorMessage="Solo se admiten numeros." ForeColor="Red" ValidationExpression="([0-9]|-)*">*</asp:RegularExpressionValidator>
    </h3>
    <h3 class="auto-style4">
        Confirma contraseña:<asp:TextBox ID="txtConfirma" runat="server" CssClass="auto-style7" MaxLength="10" TextMode="Password"></asp:TextBox>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNueva" ControlToValidate="txtConfirma" ErrorMessage="Contraseña diferente." ForeColor="Red">*</asp:CompareValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtConfirma" ErrorMessage="Repita la contraseña." ForeColor="Red">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtConfirma" ErrorMessage="Solo se admiten numeros." ForeColor="Red" ValidationExpression="([0-9]|-)*">*</asp:RegularExpressionValidator>
    </h3>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
    <p>
        <asp:Button ID="btnGuardar" runat="server" BackColor="#FF9900" CssClass="auto-style8" Text="Guardar" Width="91px" OnClick="btnGuardar_Click" />
    </p>
</asp:Content>
