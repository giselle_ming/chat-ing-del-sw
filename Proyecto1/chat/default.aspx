﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Proyecto1.chat._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-left: 54px;
        }
        .auto-style2 {
            margin-left: 38px;
        }
        .auto-style4 {
        height: 39px;
    }
        .auto-style5 {
            margin-left: 0px;
        }
        .auto-style6 {
            height: 53px;
        }
        .auto-style7 {
            margin-left: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="auto-style6">
        Ingresa al Chat</h1>
    <h3 class="auto-style4">
        Usuario:
        &nbsp;<asp:TextBox ID="txtUsuario" runat="server" CssClass="auto-style7" Width="137px" MaxLength="10"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUsuario" ErrorMessage="Caracteres invalidos." ValidationExpression="[A-Za-z a-ñ 1-9]*$" ForeColor="Red">*</asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ingrese usuario." ControlToValidate="txtUsuario" ForeColor="Red">*</asp:RequiredFieldValidator>
    </h3>
    <h3>
        Contraseña:
        <asp:TextBox ID="txtContrasena" runat="server" CssClass="auto-style5" Width="138px" MaxLength="10" TextMode="Password"></asp:TextBox>
    &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContrasena" ErrorMessage="Ingresa solo numeros." ValidationExpression="([0-9]|-)*" Display="Dynamic" ForeColor="Red">*</asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Ingrese contraseña" ControlToValidate="txtContrasena" ForeColor="Red">*</asp:RequiredFieldValidator>
    </h3>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="error" runat="server" ForeColor="Red"></asp:Label>
        </p>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableTheming="True" ForeColor="Red" />
    <p>
        <asp:Button ID="botonIngresa" runat="server" CssClass="auto-style1" Text="Ingresa" Width="79px" BackColor="#FF9900" OnClick="Button1_Click" style="height: 26px" />
        <asp:Button ID="Button2" runat="server" CssClass="auto-style2" Text="Olvide mi contraseña" Width="226px" BackColor="#FF9900" CausesValidation="False" OnClick="Button2_Click" />
    </p>
    </asp:Content>
