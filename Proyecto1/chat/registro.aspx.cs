﻿using Proyecto1.app;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto1.chat
{
    public partial class registro : System.Web.UI.Page
    {
        static Servicio gise = new Servicio();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void botonRegistro_Click(object sender, EventArgs e)
        {
            gise.crearCarpeta();
            
            Usuario gise_usuario = gise.crearUsuario(txtNombreR.Text.ToString(), txtApellidoR.Text.ToString(), txtUsuarioR.Text.ToString(), txtContrasenaR.Text.ToString(),
                              txtPreguntaR.Text.ToString(), txtRespuestaR.Text.ToString());

            if (gise_usuario != null)
            {
                Response.Redirect("creada.aspx");
            }

            else
            {
                this.cuentaExiste.Text = txtUsuarioR.Text.ToString() + " ya esta registrado/a.";
                this.txtUsuarioR.Text = "";
                this.txtNombreR.Text = "";
                this.txtApellidoR.Text = "";
                this.txtPreguntaR.Text = "";
                this.txtRespuestaR.Text = "";
            }


        }

    }
}


//Giselle Mingue