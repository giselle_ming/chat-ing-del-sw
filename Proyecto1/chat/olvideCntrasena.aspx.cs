﻿using Proyecto1.app;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto1.chat
{
    public partial class olvideCntrasena : System.Web.UI.Page
        
    {
        Servicio gise = new Servicio();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Usuario gise_user = gise.verificarUsuario(txtUsuarioRecupera.Text.ToString(), null);

            if (gise_user != null)
            {

                //nada

            }
            else
            {
                this.error.Text = "Usuario no valido";
            }

        }

        protected void botonRecuperar_Click(object sender, EventArgs e)
        {
            Session["User"] = txtUsuarioRecupera.Text.ToString();
            Response.Redirect("recuperaContrasena.aspx");
        }
    }
}

//Giselle Mingue