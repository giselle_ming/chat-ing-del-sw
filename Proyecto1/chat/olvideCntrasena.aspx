﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="olvideCntrasena.aspx.cs" Inherits="Proyecto1.chat.olvideCntrasena" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 38px;
        }
        .auto-style3 {
            height: 22px;
        }
        .auto-style5 {
            margin-left: 27px;
        }
        .auto-style7 {
            margin-left: 86px;
        }
        .auto-style8 {
            height: 53px;
        }
        .auto-style9 {
            height: 21px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="auto-style8">
        Recupera tu contraseña</h1>
    <h3 class="auto-style1">
        Usuario:<asp:TextBox ID="txtUsuarioRecupera" runat="server" CssClass="auto-style5" Width="143px"></asp:TextBox>
    </h3>
    <p class="auto-style9">
        <asp:Label ID="error" runat="server" ForeColor="Red"></asp:Label>
    </p>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
    <p class="auto-style3">
        <asp:Button ID="botonRecuperar" runat="server" BackColor="#FF9900" CssClass="auto-style7" Text="Recuperar" Width="127px" OnClick="botonRecuperar_Click" />
    &nbsp;</p>
</asp:Content>
