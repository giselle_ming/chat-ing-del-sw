﻿using Proyecto1.app;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto1.chat
{
    public partial class _default : System.Web.UI.Page
    {
        Servicio gise = new Servicio();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           

            Usuario gise_user = gise.verificarUsuario(txtUsuario.Text.ToString(), txtContrasena.Text.ToString());

            if (gise_user != null)
            {
                
                Session["Usuario"] = txtUsuario.Text.ToString();

               
                Response.Redirect("perfil.aspx");

            }
            else
            {
                error.Text = "Nombre o contraseña incorrecta";
            }
 
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("olvideCntrasena.aspx");
        }
    }
}

//Giselle Mingue