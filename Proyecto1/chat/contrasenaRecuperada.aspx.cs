﻿using Proyecto1.app; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto1.chat
{
    public partial class contrasenaNueva : System.Web.UI.Page
    {
        Servicio gise = new Servicio();

        protected void Page_Load(object sender, EventArgs e)
        {
            gise.getInfoUsuario((string)Session["User"]);


            Usuario gise_user = gise.getInfoUsuario((string)Session["User"]);

            this.contrasena.Text = gise_user.Contrasena;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
    }
}

//Giselle Mingue