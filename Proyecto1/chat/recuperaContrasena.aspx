﻿<%@ Page Title="" Language="C#" MasterPageFile="~/chat/Site1.Master" AutoEventWireup="true" CodeBehind="recuperaContrasena.aspx.cs" Inherits="Proyecto1.chat.recuperaContrasena" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-left: 11px;
        }
        .auto-style2 {
            height: 45px;
        }
        .auto-style3 {
            height: 37px;
        }
        .auto-style4 {
            height: 40px;
        }
        .auto-style5 {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="auto-style2">
        Recupera tu contraseña</h1>
    <h3 class="auto-style3">
        Pregunta secreta:
        <asp:Label ID="preguntaScr" runat="server"></asp:Label>
        ?</h3>
    <h3 class="auto-style4">
        Respuesta:<asp:TextBox ID="txtRespuesta" runat="server" CssClass="auto-style1" Width="380px"></asp:TextBox>
    </h3>
    <p class="auto-style5">
        <asp:Label ID="error" runat="server" ForeColor="Red"></asp:Label>
    </p>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
    <p>
        <asp:Button ID="Button1" runat="server" BackColor="#FF9900" Text="Continuar" OnClick="Button1_Click" />
    </p>
</asp:Content>
