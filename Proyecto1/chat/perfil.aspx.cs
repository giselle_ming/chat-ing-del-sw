﻿using Proyecto1.app;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto1.chat
{
    public partial class perfil1 : System.Web.UI.Page
    {
        Servicio gise = new Servicio();

        protected void Page_Load(object sender, EventArgs e)
        {
            gise.getInfoUsuario((string)Session["Usuario"]);
            

            Usuario gise_user = gise.getInfoUsuario((string)Session["Usuario"]);

           
            this.usuario.Text = gise_user.UsuarioNombre;
            this.nombre.Text = gise_user.Nombre;
            this.apellido.Text = gise_user.Apellido;


        }

        protected void btnModifica_Click(object sender, EventArgs e)
        {
            Response.Redirect("modificaUser.aspx");
        }

        protected void btnChat_Click(object sender, EventArgs e)
        {
            Response.Redirect("chat.aspx");
        }

        protected void btnContrasena_Click(object sender, EventArgs e)
        {
            Response.Redirect("cambiarContrasena.aspx");
        }

        protected void btnPregunta_Click(object sender, EventArgs e)
        {
            Response.Redirect("cambiarPregunta.aspx");
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {

            if (gise.borrarUsuario((string)Session["Usuario"]))
            {

                Response.Redirect("eliminada.aspx");

            }
            else
            {
                //nada
            }
        }
    }
}

//Giselle Mingue